#include <MIDI.h>

struct MySettings : public midi::DefaultSettings
{
    static const long BaudRate = 115200;
};
// Create and bind the MIDI interface to the default hardware Serial port, custom baud rate as seen above.
MIDI_CREATE_CUSTOM_INSTANCE(HardwareSerial, Serial, MIDI, MySettings);

const int LED_ON_TIME = 0; //ms
const int DELAY_TIME = ((float)LED_ON_TIME/512.0)*1000;

//Address pins
const int selectPins[3] = {2, 3, 4}; // S0~2, S1~3, S2~4

//Address pin values
int pinValues[3] = {0, 0, 0};

//Mux Common Pins
const int faders1 = A0;
const int faders2 = A1;
const int pots1 = A2;
const int pots2 = A3;
const int leds1 = 6; //Bridged on board, fix in future versions - requirese PWM, not Analog pin!!
const int leds2 = 5;

int muxChannels = 8;
int count = 0;

void setup() 
{
  MIDI.begin(MIDI_CHANNEL_OMNI);  // Listen to all incoming messages - MIDI_CHANNEL_OMNI
  Serial.begin(115200); // will change baud rate of MIDI traffic from 31250 to 115200
  // Set up pins
  for (int i=0; i<3; i++)
  {
    pinMode(selectPins[i], OUTPUT);
    digitalWrite(selectPins[i], LOW);
  }
  pinMode(leds1, OUTPUT);
  pinMode(leds2, OUTPUT);
  analogWrite(leds1, LOW);
  analogWrite(leds2, LOW);

  pinMode(faders1, INPUT);
  pinMode(faders2, INPUT);
  pinMode(pots1, INPUT);
  pinMode(pots2, INPUT);

}

void loop() 
{
  
  //MIDI READ
  for (int pin=0; pin<muxChannels; pin++)
  {
    selectMuxPin(pin);
//    if (MIDI.read(1))
//    {
      int fader1 = map(analogRead(faders1)/8, 0, 127, 0, 127);
      int fader2 = map(analogRead(faders2)/8, 0, 127, 0, 127);
      int pot1 = map(analogRead(pots1)/8, 0, 127, 127, 0);
      int pot2 = map(analogRead(pots2)/8, 0, 127, 127, 0);
      MIDI.sendControlChange((pin + 1), fader1, 1);
      MIDI.sendControlChange((pin + 17), pot1, 1);
      MIDI.sendControlChange((pin + 9), fader2, 1);
      MIDI.sendControlChange((pin + 25), pot2, 1);
      analogWrite(leds1, 0);
      analogWrite(leds1, matchFader(faders1));
      delay(2);
      analogWrite(leds1, 0);
      analogWrite(leds2, 0);
      analogWrite(leds2, matchFader(faders2));
      delay(2);
      analogWrite(leds2, 0);
//    }
  }
}

// selectMuxPin function sets the S0, S1, and S2 pins according to pin from 0-muxChannels-1.
void selectMuxPin(byte pin)
{
  if (!(pin < muxChannels)) return; // Exit if pin is out of scope
  for (int i=0; i<3; i++)
  {
    pinValues[i] = bitRead(pin, i);
    digitalWrite(selectPins[i], pinValues[i]);
  }
}

// Match LED intensity to Fader position -- LEDs - 0-255, Faders - 0-1023
int matchFader(int fader)
{
  int intensity = 0;
  int reading = analogRead(fader);
  intensity = reading/4;
  return intensity;
}
