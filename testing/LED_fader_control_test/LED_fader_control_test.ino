//Address pins
const int selectPins[3] = {2, 3, 4}; // S0~2, S1~3, S2~4

//Address pin values
int pinValues[3] = {0, 0, 0};

//Mux Common Pins
const int faders1 = A0;
const int faders2 = A1;
const int pots1 = A2;
const int pots2 = A3;
const int leds1 = 6; //Bridged on board, fix in future versions - requirese PWM, not Analog pin!!
const int leds2 = 5;

int muxChannels = 8;
int count = 0;

void setup() 
{
  // Set up pins
  for (int i=0; i<3; i++)
  {
    pinMode(selectPins[i], OUTPUT);
    digitalWrite(selectPins[i], LOW);
  }
  pinMode(leds1, OUTPUT);
  pinMode(leds2, OUTPUT);
  analogWrite(leds1, LOW);
  analogWrite(leds2, LOW);

  pinMode(faders1, INPUT);
  pinMode(faders2, INPUT);
  pinMode(pots1, INPUT);
  pinMode(pots2, INPUT);

}

void loop() 
{
  for (count=0; count<=7; count++)
  {
    //select mux bit
    selectMuxPin(count);
    for (int intensity=0; intensity<=matchFader(faders1); intensity = intensity + 255)
    {
      analogWrite(leds1, intensity);
    }
    for (int intensity=0; intensity<=matchFader(faders2); intensity = intensity + 255)
    {
      analogWrite(leds2, intensity);
    }
  }
}

// The selectMuxPin function sets the S0, S1, and S2 pins
// accordingly, given a pin from 0-7.
void selectMuxPin(byte pin)
{
  if (!(pin < muxChannels)) return; // Exit if pin is out of scope
  for (int i=0; i<3; i++)
  {
    pinValues[i] = bitRead(pin, i);
    digitalWrite(selectPins[i], pinValues[i]);
  }
}

// Match LED intensity to Fader position -- LEDs - 0-255, Faders - 0-1023
int matchFader(int fader)
{
  int intensity = 0;
  int reading = analogRead(fader);
  intensity = reading/4;
  return intensity;
}
