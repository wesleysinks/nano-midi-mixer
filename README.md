# Nano Midi Mixer

A small desktop midi control surface for mixing, built around an Arduino Nano

## Limitations/Notes:
* Since the control surface is based on the Arduino Nano board, there is no native USB MIDI functionality. It uses the standard MIDI library over Serial via [ttymidi](http://www.varal.org/ttymidi/) (Linux CLI only) or [The Hairless MIDI to Serial Bridge](https://projectgus.github.io/hairless-midiserial/) (cross-platform GUI)

* Tested and working with Purr Data/Pure Data & Ardour 6. Ardour 6 MIDI mapping file included in /extra/ directory.

## TODO:
* Move LED mux commons to PWM pins instead of analog pins.
* Update design to interface with a board that includes native USB MIDI

![Nano Midi Mixer PCB](img/nano-midi-mixer-v1.png)
![Schematic](img/nanomidimixer.svg)
